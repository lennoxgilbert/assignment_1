## Weather-App

Created by © Lennox Gilbert 2021

Weatherapp is a simple weather forecast app, which uses some APIs to fetch the current forecast of the weather of places, cities,counties from around the world. The main goal of this app is to demonstrate using the fetch API as an interface for getting information from resources about the internet.

## Running Application

Due to the simplicity of the application, this can simply be by navigating to the `index.html` and opening the file in the browser

## Technologies Used

HTML, CSS and Javascript

## Design

Inspired by [Ramonyv](https://www.uplabs.com/posts/weather-app-freebie) and weather icons taken from [isneezy/open-weather-icons](https://github.com/isneezy/open-weather-icons)
